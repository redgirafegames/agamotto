# README #

### AGAMOTTO ###

Agamotto is an Open Source Time Manipulating Tool for Unity.

It allows you to store your objects state, would it be recording their past or simulating their future. You can then move on the timeline and visualize their time data with ease.

### Content ###
To simplify the code maintenance, the repository is a complete Unity project ready to start.
Agamotto code is located in : *Assets/RedGirafeGames/Agamotto*
You can also import Agamotto from the asset store : https://u3d.as/260R

### Documentation ###

Please see the online documentation for videos, tutorials and API details :
https://www.redgirafegames.com/agamotto.html


### License ###
[MIT](https://choosealicense.com/licenses/mit/)