﻿using System.Collections;
using System.Collections.Generic;
using RedGirafeGames.Agamotto.Scripts.Runtime;
using RedGirafeGames.Agamotto.Scripts.Runtime.Agents;
using RedGirafeGames.Agamotto.Scripts.Runtime.Utils;
using UnityEngine;

public class MyCode : MonoBehaviour
{
    public TimeStone _timeStone;
    private TimeAgent _timeAgent;
    // Start is called before the first frame update
    void Start()
    {
        _timeStone.onSimulationSceneReady.AddListener(StoneSimReady);
        _timeAgent = GetComponent<TimeAgent>();
        // _timeAgent.onInitTimeAgentsList.AddListener(OnInitList);
        // _timeAgent.onClonedForSimulation.AddListener(OnCloned);
        // _timeAgent.onSimulationStart.AddListener(OnStart);
        // _timeAgent.onSimulationEarlyUpdate.AddListener(OnEarly);
        // _timeAgent.onSimulationUpdate.AddListener(OnUpdate);
        // _timeAgent.onSimulationFixedUpdate.AddListener(OnFixed);
        // _timeAgent.onSimulationLateUpdate.AddListener(OnLate);
        // _timeAgent.onPersistTick.AddListener(OnPersist);
        // _timeAgent.onTimeLineChange.AddListener(OnTlChange);
        // _timeAgent.onSimulationComplete.AddListener(OnComp);
        // _timeAgent.onSetDataTick.AddListener(OnSetData);
    }

    private void StoneSimReady()
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnStoneSimReady");
    }

    private void OnTlChange(TimeStone arg0, TimeStone.TimeTickOrigin arg1)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnTimeLineChange");
    }

    private void OnLate(TimeStone arg0, float arg1)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnLate");
    }

    private void OnFixed(TimeStone arg0, float arg1)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnFixed");
    }

    private void OnEarly(TimeStone arg0, float arg1)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnEarly");
    }

    private void OnStart(TimeStone arg0)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnStart");
    }

    private void OnSetData(TimeStone arg0, int arg1)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnSetData");
    }

    private void OnComp(TimeStone arg0)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnComplete");
        Debug.Log("GO hash : " + _timeAgent.GetHashCode());
    }

    private void OnPersist(TimeStone arg0, TimeStone.TimeTickOrigin arg1, float arg2)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnPersist");
    }

    private void OnInitList(TimeStone arg0)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnInitList");
    }

    private void OnCloned(TimeStone arg0)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnCloned");
    }

    private void OnSimReady(TimeStone arg0)
    {
        Debug.Log(_timeAgent.SimulationClone);
        Debug.Log("C " + _timeAgent.IsClone + " > OnSimReady");
    }

    private void OnUpdate(TimeStone arg0, float arg1)
    {
        Debug.Log("C " + _timeAgent.IsClone + " > OnUpdate");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
