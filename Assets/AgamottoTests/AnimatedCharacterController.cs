﻿using RedGirafeGames.Agamotto.Scripts.Runtime;
using UnityEngine;

namespace RedGirafeGames.Agamotto.Demo.Scripts
{
    public class AnimatedCharacterController : MonoBehaviour
    {
        private Game _game;

        private CharacterController _controller;
        private Vector3 _playerVelocity;
        private float playerSpeed = 2.0f;
        private float jumpHeight = 10.0f;
        private float gravityValue = -9.81f;

        public bool _jumpAsked = false;
        private bool _jumping = false;

        private Animator _animator;
        private static readonly int AnimatorSpeedParam = Animator.StringToHash("Speed");
        private static readonly int AnimatorJumpingParam = Animator.StringToHash("Jumping");

        private void Start()
        {
            _game = FindObjectOfType<Game>();
            _controller = GetComponent<CharacterController>();
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {
            if (_game.autoSimule)
            {
                OnUpdate(Time.deltaTime, null);
            }
            else
            {
                _controller.enabled = false;
                _animator.speed = 0.0f;
            }
        }

        public void OnUpdate(float step, TimeStone stone)
        {
            _animator.speed = 1.0f;
            _controller.enabled = true;
        
            if (_jumping && transform.position.y <= 0)
            {
                _jumping = false;
                _animator.SetBool(AnimatorJumpingParam, false);
                _playerVelocity.y = 0;
            }

            Vector3 move = transform.forward;
            _controller.Move(move * step * playerSpeed);

            _animator.SetFloat(AnimatorSpeedParam, playerSpeed);

            // Changes the height position of the player..
            if (_jumpAsked)
            {
                _jumping = true;
                _animator.SetBool(AnimatorJumpingParam, true);
                _playerVelocity.y = Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
                _jumpAsked = false;
            }

            _playerVelocity.y += gravityValue * Time.deltaTime;
            _controller.Move(_playerVelocity * Time.deltaTime);
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            if (hit.gameObject.CompareTag("Obstacle"))
            {
                _jumpAsked = true;
            }
        }
    }
}