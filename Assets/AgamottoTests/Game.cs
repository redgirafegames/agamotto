﻿using System.Threading;
using RedGirafeGames.Agamotto.Scripts.Runtime;
using RedGirafeGames.Agamotto.Scripts.Runtime.Utils;
using UnityEngine;

namespace RedGirafeGames.Agamotto.Demo
{
    public class Game : MonoBehaviour
    {
        public bool autoSimule = false;
        
        public TimeStone stone;

        // Start is called before the first frame update
        void Start()
        {
            // Physics.autoSimulation = autoSimule;
            time = stone.simulationDuration;
            Physics.autoSimulation = autoSimule;
        }

        // Update is called once per frame
        void Update()
        {
        }

        public float time = 1;
        public TimeUtils.RigidbodyFreezeType freezeType = TimeUtils.RigidbodyFreezeType.None;
        public void Sim(bool async)
        {
            stone.StartSimulation(async);
            Freeze();
        }

        public void Freeze()
        {
            stone.FreezeTimeAgents(true, freezeType);
        }

        public void UnFreeze()
        {
            stone.FreezeTimeAgents(false, freezeType);
        }
        
        public void LongOnUpdate(int sleepTime)
        {
            Thread.Sleep(sleepTime);
        }

        public void OnUpdateTest()
        {
            Debug.Log("THIS IS ON UPDATE TEST");
        }

        public void OnSimulationCompleteTest()
        {
            Debug.Log("SIMULATION COMPLETE TEST");
        }
    }
}
