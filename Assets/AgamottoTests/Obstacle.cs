﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using RedGirafeGames.Agamotto.Demo;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Obstacle : MonoBehaviour
{
    private Game _game;
    public ParticleSystem ps;

    // Start is called before the first frame update
    void Awake()
    {
        Physics.autoSimulation = true;
        _game = FindObjectOfType<Game>();
        if (ps == null)
            ps = FindObjectOfType<ParticleSystem>();
    }

    private void Start()
    {
        ps.Stop(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (_game.autoSimule)
            ps.Play(true);
        else
        {
            ps.Pause(true);
        }
    }

    public void SimParts(float step)
    {
        Stopwatch w = new Stopwatch();
        w.Start();
        if (ps.isStopped)
        {
            Debug.Log("playback");
            ps.Play();
        }

        ps.Simulate(step, true, false, false);

        w.Stop();
        Debug.Log("time to simulate : " + w.ElapsedMilliseconds);
    }
}