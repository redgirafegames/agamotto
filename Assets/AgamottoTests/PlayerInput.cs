﻿using RedGirafeGames.Agamotto.Scripts.Runtime.Utils;
using UnityEngine;

namespace RedGirafeGames.Agamotto.Demo.Scripts
{
    public class PlayerInput : MonoBehaviour
    {
        private Game _game;
        void Start()
        {
            _game = FindObjectOfType<Game>();
        }

        void Update()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                _game.autoSimule = !_game.autoSimule;
            }
            else if (Input.GetButtonDown("Fire2"))
            {
                _game.Sim(false);
            }
        }
    }
}