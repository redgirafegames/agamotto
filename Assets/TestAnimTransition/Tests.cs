﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tests : MonoBehaviour
{
    public Animator animator;

    public bool takeSnapshot;
    private bool snapshotDone;

    public bool applySnapshot;
    private bool snapshotApplied;

    public bool toggleAnimator;
    private int currentHash;
    private float currentTime;
    private int layer = 0;
    private float normalizedTime;
    private int nextHash;
    private float nextLength;
    private float transitionTime;
    private float transitionLength;
    private bool inTransition;
    private float transitionNormalizedTime;
    private float transitionDuration;
    private float nextNormalizedTime;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        animator.speed = toggleAnimator ? 1.0f : 0.0f;
        if (takeSnapshot && !snapshotDone)
        {
            Debug.Log("Take snap");
            snapshotDone = true;
            TakeSnap();
            toggleAnimator = false;
            applySnapshot = false;
            snapshotApplied = false;
        }

        else if (applySnapshot && !snapshotApplied)
        {
            Debug.Log("Apply snap");
            snapshotApplied = true;
            ApplySnap();
            toggleAnimator = false;
            takeSnapshot = false;
            snapshotDone = false;
        }
    }

    private void ApplySnap()
    {
        if (inTransition)
        {
            animator.Play(currentHash, layer, normalizedTime);
            animator.Update(0.0f);
            animator.CrossFade(nextHash, transitionDuration, layer, nextNormalizedTime,
                transitionNormalizedTime);
        }
        else
        {
            animator.Play(currentHash, layer, normalizedTime);
        }
    }

    private void TakeSnap()
    {
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(layer);
        currentHash = info.fullPathHash;
        normalizedTime = info.normalizedTime;

        inTransition = animator.IsInTransition(layer);
        if (inTransition)
        {
            Debug.Log("In transition...");

            AnimatorTransitionInfo transInfo = animator.GetAnimatorTransitionInfo(layer);
            AnimatorStateInfo nextInfo = animator.GetNextAnimatorStateInfo(layer);

            transitionDuration = transInfo.duration;
            transitionNormalizedTime = transInfo.normalizedTime;
            nextHash = nextInfo.fullPathHash;
            nextNormalizedTime = nextInfo.normalizedTime;
        }
    }
}